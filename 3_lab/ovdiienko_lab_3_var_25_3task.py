from random import random
vector = []
i=0
def checker(prompt: str) -> int:
    while True:
        try:
            value1 = int(input(prompt))
            value2 = int(input(prompt))
        except ValueError:
            print("Sorry, you wrote not a value data")
            continue

        if value1 > 100:
            print("Sorry, n must be less than 100")
            continue
        elif value2 > 200:
            print("Sorry, m must be less than 200")
            continue
        else:
            break
    return value1, value2

def checker2(prompt: str, size: int) -> int:
    while True:
        try:
            value = int(input(prompt))
        except ValueError:
            print("Sorry, you wrote not a value data")
            continue

        if value > size:
            print("Sorry, you're out of row")
            continue
        else:
            break
    return value
n, m = checker("Please enter value: ")
print(n)
print(m)

for i in range(n):
    vector.append([])
    for j in range(m):
        vector[i].append(int(random()*150))

print(vector)

def flip(vct, pos):
    flipped = vct[pos][::-1]
    return flipped

position = checker2("Enter the row you want to be flipped ", n)

flippy = flip(vector, position)
vector.pop(position)
vector.insert(position, flippy)

print(vector)