from random import random
vector = []
i=0
def checker(prompt: str) -> int:
    while True:
        try:
            value = int(input(prompt))
        except ValueError:
            print("Sorry, you wrote not a value data")
            continue

        if value > 100:
            print("Sorry, n must be less than 100")
            continue
        else:
            break
    return value
n = checker("Please enter n: ")

while (i < n):
    rnd = int(random() * 150)
    vector.append(rnd)
    i += 1

print(vector)

def flip(vct):
    flipped = vct[::-1]
    return flipped

print(flip(vector))
