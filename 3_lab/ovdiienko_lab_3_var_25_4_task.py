import random
first = tuple([int((0) + (5 - (0)) * random.random()) for _ in range(10)])
print(first)
second = tuple([int((-5) + (5 - (-5)) * random.random()) for _ in range(10)])
print(second)
third = first + second
print(third)
print(f"Amount of zeros: {third.count(0)}")