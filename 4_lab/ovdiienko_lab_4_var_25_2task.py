import collections
thestring = input()
d = collections.defaultdict(int)
for c in thestring:
    d[c] += 1

def finder_less_using_symbol(collection):
    for c in sorted(collection, key=collection.get, reverse=False):
        symbol = c
        amount = collection[c]
        break
    return symbol, amount

sym, amo = finder_less_using_symbol(d)

print(f"The less common character in the text is {sym} and it repeats {amo} times")
