print("Enter a value")
a = float(input())
print("Enter b value")
b = float(input())
print("Enter c value")
c = float(input())
print("Enter d value")
d = float(input())

if a<=b<=c<=d:
    def max_variable(a: float, b: float, c: float, d: float) -> float:
        return max(max(max(a, b), c), d)
    maximum = max_variable(a, b, c, d)
    a=maximum
    b=maximum
    c=maximum
    d=maximum

elif a>b>c>d:
    pass

else:
    a=a*a
    b=b*b
    c=c*c
    d=d*d

print(f"Variables after conditions {a}, {b}, {c}, {d}")

