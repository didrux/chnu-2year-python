import math

def f(a: float, b: float) -> float:
    y = pow((1.56*math.sqrt(float(math.sin(b))) / 0.8942*math.log(a, math.e)), 1/4)
    return y

print("Enter 'a' number equals 3")
a = float(input())

print("Enter 'b' number equals 0.523")
b = float(input())

print(f"Result is {f(a,b)}")



