import math

def summa(a: float, epsilon: float) -> float:
    if a == 0 or epsilon <= 0:
        print("You wrote the wrong variable values")
        return
    k = 1
    factorial = 1
    sum1 = ((pow((math.e), -k))/((pow(a, 2*k))+(factorial)))
    sum2 = 0
    while abs(sum2 - sum1) > epsilon:
        k += 1
        factorial *= k
        sum2 = sum1
        sum1 += ((pow((math.e), -k))/((pow(a, 2*k))+(factorial)))
        print(k, factorial, sum1, sum2, abs(sum2 - sum1))
    return sum1, k, epsilon

print("Input a and epsilon variables, please: ")
a=float(input())
epsilon=float(input())
print(f"Summa, amount of terms, epsilon equals = {summa(a,epsilon)}")