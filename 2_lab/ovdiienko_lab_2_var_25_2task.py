import math

def progression(x: float, epsilon: float) -> float:
    if epsilon <= 0:
        print("You wrote the wrong variable values")
        return
    n = 1
    sequence1 = (((pow(x, 2*n))*(math.sin(pow(x, n))))/(math.factorial(pow(n, 2))))
    sequence2 = 0
    while abs(sequence1 - sequence2) > epsilon and n <= 100:
        n += 1
        sequence2 = sequence1
        sequence1 = (((pow(x, 2*n))*(math.sin(pow(x, n))))/(math.factorial(pow(n, 2))))
        print(n, math.factorial(pow(n, 2)), sequence1, sequence2, abs(sequence1 - sequence2))
    return sequence1, n

print("Input x and epsilon variables, please: ")
x=float(input())
epsilon=float(input())
print(f"a(n), number equals = {progression(x, epsilon)}")